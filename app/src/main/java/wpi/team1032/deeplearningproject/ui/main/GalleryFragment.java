package wpi.team1032.deeplearningproject.ui.main;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import wpi.team1032.deeplearningproject.R;
import wpi.team1032.deeplearningproject.data.sqlite.GalleryDbHelper;

public class GalleryFragment extends Fragment {
    private static final int REQUEST_IMAGE_CAPTURE = 0;
    int SPAN_COUNT = 2;

    private GalleryViewModel mViewModel;
    private RecyclerView gallery;
    private FloatingActionButton fab;

    private String mImagePath;

    View.OnClickListener galleryItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int itemPos = gallery.getChildLayoutPosition(v);

            // Create new fragment
            Fragment newFrag = PictureInferFragment.newInstance(itemPos);
            FragmentTransaction trans = getFragmentManager().beginTransaction();
            // Replace current fragment (main fragment)
            trans.replace(R.id.container, newFrag, "PicLayout");
            // Save to back stack
            trans.addToBackStack(null);
            // Commit the transaction
            trans.commit(); /**/
        }
    };

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gallery_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        fab = getActivity().findViewById(R.id.floatingActionButton);

        // TODO: Use the ViewModel

        // Setup recycler view (gallery)
        gallery = (RecyclerView) getActivity().findViewById(R.id.imgGallery);
        gallery.setHasFixedSize(true);

        gallery.setLayoutManager(
                new GridLayoutManager(getActivity().getApplicationContext(), SPAN_COUNT));
        gallery.setAdapter(
                new GalleryAdapter(getActivity().getApplicationContext(), galleryItemClickListener)
        );

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                List<ResolveInfo> activities = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, 0);
                if (activities != null) {
                    File pFile = null;
                    try {
                        pFile = createImageFile();
                    } catch (IOException ioe){
                        // Error occurred
                        return;
                    }
                    // if succeeded
                    if (pFile != null) {
                        // Uri
                        Uri pURI = FileProvider.getUriForFile(getContext(), "wpi.team1032.deeplearningproject.fileprovider", pFile);
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, pURI);
                        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    }
                }
            }
        });

    }
    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);

        mImagePath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode != Activity.RESULT_OK){
            return;
        }

        if(requestCode == REQUEST_IMAGE_CAPTURE){
            //String[] tPath = mCurrentPhotoPath.split("/");
            //mainTextView.setText(tPath[tPath.length-1]);
            //mainImageView.setImageBitmap(BitmapFactory.decodeFile(mCurrentPhotoPath));
            Bitmap b = BitmapFactory.decodeFile(mImagePath);
            mImagePath = "";
            GalleryDbHelper galleryDb = GalleryDbHelper.getInstance();
            galleryDb.insertImg(b);
        }
    }
}
