package wpi.team1032.deeplearningproject.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;


import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.tensorflow.lite.Interpreter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import wpi.team1032.deeplearningproject.R;
import wpi.team1032.deeplearningproject.data.sqlite.GalleryContract;
import wpi.team1032.deeplearningproject.data.sqlite.GalleryDbHelper;

public class PictureInferFragment extends Fragment {

    private PictureInferViewModel mViewModel;

    int picIndex;

    int totalRatings = 0;
    float avgRating = 0;
    float lastRating = 0;
    boolean rateChanged = false;

    ImageView imageView;
    RatingBar ratingBar;
    TextView labelView;
    TextView ratingView;
    Button inferenceButton;

    Bitmap img;

    MappedByteBuffer tfliteModel = null;
    Interpreter tflite = null;

    float IMAGE_STD = 128;

    String[] labelArray = new String[1001];
    float[][] labelProbArray = new float [1][1001];

    ByteBuffer imgData;

    boolean onDevice = true;
    private String hosturl = "http://35.243.243.163:54321/inception";
    //private String hosturl = "127.0.0.1:5000/inception";
    OkHttpClient client = new OkHttpClient();

    public static PictureInferFragment newInstance(int picPos) {
        PictureInferFragment newFrag = new PictureInferFragment();

        Bundle args = new Bundle();
        args.putInt("picIndex", picPos);
        newFrag.setArguments(args);

        return newFrag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        picIndex = getArguments().getInt("picIndex");
        return inflater.inflate(R.layout.picture_infer_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(PictureInferViewModel.class);
        // TODO: Use the ViewModel

        imageView = getActivity().findViewById(R.id.imageView);
        ratingBar = getActivity().findViewById(R.id.ratingBar);
        labelView = getActivity().findViewById(R.id.labelView);
        ratingView = getActivity().findViewById(R.id.ratingView);
        inferenceButton = getActivity().findViewById(R.id.inferenceButton);

        // Load image
        Cursor cursor = GalleryDbHelper.getInstance().getImgFromIndex(picIndex);
        if(cursor.moveToFirst()) {
            img = GalleryDbHelper.getInstance().parseImg(
                    cursor.getBlob(
                            cursor.getColumnIndex(
                                    GalleryContract.GalleryTable.COLUMN_NAME_IMG)));
            float ratingVal = cursor.getFloat(cursor.getColumnIndex(GalleryContract.GalleryTable.COLUMN_NAME_RATING));
            imageView.setImageBitmap(img);
            if(ratingVal != -1) {
                if(ratingVal >= 0 && ratingVal <= 5) {
                    lastRating = ratingVal;
                    rateChanged = true;
                    avgRating = lastRating;
                    totalRatings = 1;
                    ratingBar.setRating(ratingVal);
                }
            }
        }

        // Set starting values
        labelView.setText(R.string.DEF_ON_DEV_LABEL_LOAD_TXT);
        inferenceButton.setText(R.string.DEF_ON_DEV_BTN_TXT);
        if (rateChanged)
            ratingView.setText("Current Rating: " + String.valueOf(avgRating));
        else
            ratingView.setText("Unrated");

        imgData = ByteBuffer.allocateDirect(1 * 299 * 299 * 3 * 4);
        imgData.order(ByteOrder.nativeOrder());
        try {
            tfliteModel = loadModelFile();
            tflite = new Interpreter(tfliteModel);
        }catch (IOException ioe){
            IOException e = ioe;
        }

        readLabels();

        if (rateChanged)
            ratingBar.setRating(lastRating);

        inferenceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inferenceButton.getText().toString().equals(getString(R.string.DEF_ON_DEV_BTN_TXT))){
                    if (labelView.getText().toString().equals(getString(R.string.DEF_ON_DEV_LABEL_LOAD_TXT))){
                        labelView.setText(getString(R.string.DEF_OFF_DEV_LABEL_LOAD_TXT));
                    }
                    inferenceButton.setText(getString(R.string.DEF_OFF_DEV_BTN_TXT));
                    onDevice = false;
                }
                else if (inferenceButton.getText().toString().equals(getString(R.string.DEF_OFF_DEV_BTN_TXT))){
                    if (labelView.getText().toString().equals(getString(R.string.DEF_OFF_DEV_LABEL_LOAD_TXT))){
                        labelView.setText(getString(R.string.DEF_ON_DEV_LABEL_LOAD_TXT));
                    }
                    inferenceButton.setText(getString(R.string.DEF_ON_DEV_BTN_TXT));
                    onDevice = true;
                }
                runInference();
            }
        });

        ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                lastRating = rating;
                rateChanged = true;

                ratingView.setText("Current Rating: " + String.valueOf(lastRating));
                if(GalleryDbHelper.getInstance().updateRating(picIndex, lastRating) != 1) {
                    Log.w("PictureInferFragment",
                            "updateRating() Did not update exactly one column for img with index: "
                                    + picIndex);
                }
                // TODO save rating
            }
        });
        runInference();

    }

    private class RunInferenceAsync extends AsyncTask<Bitmap, Float, Long> {
        String results;
        String time;
        Response response;

        protected void onPreExecute() {
            // Stuff to do before inference starts
        }

        protected Long doInBackground(Bitmap... imgs) {
            try {
                long startTime = System.currentTimeMillis();

                File outputDir = getActivity().getCacheDir();
                File outputFile = File.createTempFile("tmp_img", ".jpg", outputDir);

                OutputStream fileOS = new FileOutputStream(outputFile);
                imgs[0].compress(Bitmap.CompressFormat.PNG, 100, fileOS);

                fileOS.flush();
                fileOS.close();

                // Do inference here!
                //File file = new File(img_path);
                RequestBody requestBody = new MultipartBuilder()
                        .type(MultipartBuilder.FORM)
                        .addFormDataPart("file", "tmp_img.jpg", RequestBody.create(MediaType.parse("image/jpeg") , outputFile))
                        .build();

                Request request = new Request.Builder()
                        .url(hosturl)
                        .post(requestBody)
                        .build();


                response = client.newCall(request).execute();
                boolean success = response.isSuccessful();
                if (success) {
                    results = response.body().string();
                }
                time = (System.currentTimeMillis() - startTime) +"ms";

            }catch (IOException e){
                IOException ie = e;
            }


            return null;
        }

        protected void onPostExecute(Long result){
            // Stuff to do after inference ends
            Long res = result;
            if(results == null)
                results = "Async Failure";

            labelView.setText(results);
            //mIntervalView.setText(time);
        }

    }

    private MappedByteBuffer loadModelFile() throws IOException {
        AssetFileDescriptor fileDescriptor = getActivity().getAssets().openFd(getModelPath());
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    private String getModelPath() {
        return "model/inception_v3.tflite";
    }

    private void convertBitmapToByteBuffer(Bitmap bitmap){
        int [] intValues = new int[299 * 299];

        if(imgData == null){
            return;
        }
        imgData.rewind();
        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        // Convert image to floating point
        int pixel = 0;
        for (int i = 0; i < 299; ++i){
            for (int j = 0; j < 299; ++j){
                final int val = intValues[pixel++];
                addPixelValue(val);
            }
        }
    }

    protected void addPixelValue(int pixelValue){
        int sto = (pixelValue >> 16);
        int hex = 0xFF;
        float stt = (sto & hex);
        double first = ((stt - 128) / 128.0);
        imgData.putFloat((float)first);
        int tsto = (pixelValue >> 8);
        int thex = 0xFF;
        float tstt = (tsto & thex);
        double tfirst = ((tstt - 128) / 128.0);
        imgData.putFloat((float)tfirst);
        int hsto = (pixelValue);
        int hhex = 0xFF;
        float hstt = (hsto & hhex);
        float hfirst = (float) ((hstt - 128) / 128.0);
        imgData.putFloat(hfirst);
    }

    private int getMaxIndex(float[] listF){
        if (listF.length == 0){
            return -1;
        }
        int max = 0;
        for (int i = 1; i < listF.length; i++){
            if(listF[max]<listF[i])
                max=i;
        }
        return max;
    }

    private void readLabels(){
        try {
            // Open file
            InputStream fileIS = getActivity().getAssets().open("model/labels.txt");
            if (fileIS != null) {
                BufferedReader br = new BufferedReader(new InputStreamReader(fileIS));
                String line = "";
                int i = 0;
                // Read line by line
                do {
                    line = br.readLine();
                    labelArray[i] = line;
                    i++;
                } while (line != null);
                fileIS.close();
            }
        } catch (Exception e) {

        }
    }

    private void runInference(){
        if(onDevice == true) {
            labelView.setText("Processing...");
            long startTime = System.currentTimeMillis();
            convertBitmapToByteBuffer(Bitmap.createScaledBitmap(img, 299, 299, true));
            labelProbArray = new float[1][1001];
            tflite.run(imgData, labelProbArray);
            int max = getMaxIndex(labelProbArray[0]);
            labelView.setText(labelArray[max] + ": " + labelProbArray[0][max] * 100 + "%");
            long timeInterval = System.currentTimeMillis() - startTime;
            //mIntervalView.setText(timeInterval + "ms");
        }
        else{
            labelView.setText("Processing...");
            //mIntervalView.setText("Processing...");
            new RunInferenceAsync().execute(img);
        }
    }

}
