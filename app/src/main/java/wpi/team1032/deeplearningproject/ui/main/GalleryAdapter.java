package wpi.team1032.deeplearningproject.ui.main;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import wpi.team1032.deeplearningproject.R;
import wpi.team1032.deeplearningproject.data.sqlite.GalleryContract;
import wpi.team1032.deeplearningproject.data.sqlite.GalleryDbHelper;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {
    GalleryDbHelper galleryDb;
    private Context context;
    View.OnClickListener galleryItemClickListener;

    public GalleryAdapter(Context context, View.OnClickListener galleryItemClickListener) {
        galleryDb = GalleryDbHelper.getInstance();
        this.context = context;
        this.galleryItemClickListener = galleryItemClickListener;
    }

    @NonNull
    @Override
    public GalleryAdapter.GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_cell_layout, viewGroup, false);
        view.setOnClickListener(galleryItemClickListener);
        return new GalleryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.GalleryViewHolder galleryViewHolder, int i) {
        String rating = "unrated";
        Cursor cursor = galleryDb.getImgFromIndex(i);
        Bitmap img = null;
        if(cursor.moveToFirst()) {
            img = galleryDb.parseImg(
                    cursor.getBlob(
                            cursor.getColumnIndex(
                                    GalleryContract.GalleryTable.COLUMN_NAME_IMG)));
            float ratingVal = cursor.getFloat(cursor.getColumnIndex(GalleryContract.GalleryTable.COLUMN_NAME_RATING));
            if(ratingVal != -1) {
                if(ratingVal >= 0 && ratingVal <= 5) {
                    rating = "Rating: " + Float.toString(ratingVal);
                }
            }
        }

        galleryViewHolder.rating.setText(rating);
        galleryViewHolder.img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        galleryViewHolder.img.setImageBitmap(img);
    }

    @Override
    public int getItemCount() {
        return (int) galleryDb.getGalleryCount();
    }

    public class GalleryViewHolder extends RecyclerView.ViewHolder {
        private TextView rating;
        private ImageView img;

        public GalleryViewHolder(@NonNull View itemView) {
            super(itemView);

            rating = (TextView) itemView.findViewById(R.id.rating);
            img = (ImageView) itemView.findViewById(R.id.img);
        }
    }
}