package wpi.team1032.deeplearningproject.data.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

public class GalleryDbHelper extends SQLiteOpenHelper {
    private static GalleryDbHelper sInstance;

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "GalleryTable.db";

    public static synchronized GalleryDbHelper getInstance(Context context) {
        if(sInstance == null) {
            sInstance = new GalleryDbHelper(context.getApplicationContext());
            if(sInstance.getGalleryCount() <= 0) {
                sInstance.initDb(context);
            }
        }
        return sInstance;
    }

    public static GalleryDbHelper getInstance() {
        return sInstance;
    }

    private GalleryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(GalleryContract.SQL_CREATE_GALLERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Recreate Database
        db.execSQL(GalleryContract.SQL_DELETE_GALLERY);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    // Helpers
    private void initDb(Context context) {
        try {
            ArrayList galleryList = new ArrayList(Arrays.asList(context.getAssets().list("cats")));
            InputStream img_stream = null;
            for(Object item : galleryList) {
                img_stream = context.getAssets().open("cats/" + item);
                insertImg(BitmapFactory.decodeStream(img_stream));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Wrappers
    public Cursor getAllImgs() {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + GalleryContract.GalleryTable.TABLE_NAME, null);
    }

    public Cursor getImgFromIndex(int index) {
        SQLiteDatabase db = sInstance.getReadableDatabase();

        String[] proj = {
                GalleryContract.GalleryTable._ID,
                GalleryContract.GalleryTable.COLUMN_NAME_IMG,
                GalleryContract.GalleryTable.COLUMN_NAME_RATING
        };

        String sel = GalleryContract.GalleryTable._ID + " =? ";
        String[] selArgs = {String.valueOf(index+1)};

        String sortOrder = GalleryContract.GalleryTable._ID + " DESC";

        return db.query(
                GalleryContract.GalleryTable.TABLE_NAME,
                proj, sel, selArgs, null, null, sortOrder
        );
    }

    public long getGalleryCount() {
        return DatabaseUtils.queryNumEntries(sInstance.getReadableDatabase(),
                GalleryContract.GalleryTable.TABLE_NAME);
    }

    public void insertImg(Bitmap img) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        byte[] imgData = outputStream.toByteArray();
        ContentValues cv = new ContentValues();
        cv.put(GalleryContract.GalleryTable.COLUMN_NAME_IMG, imgData);
        sInstance.getWritableDatabase().insert(
                GalleryContract.GalleryTable.TABLE_NAME, null, cv);
    }

    public int updateRating(int picIndex, float newRating) {
        SQLiteDatabase db = sInstance.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(GalleryContract.GalleryTable.COLUMN_NAME_RATING, newRating);

        String sel = GalleryContract.GalleryTable._ID + " = ?";
        String[] selArgs = {String.valueOf(picIndex+1)};

        return db.update(
                GalleryContract.GalleryTable.TABLE_NAME,
                cv, sel, selArgs);
    }

    public Bitmap parseImg(byte[] imgData) {
        return BitmapFactory.decodeByteArray(imgData, 0, imgData.length);
    }

}
