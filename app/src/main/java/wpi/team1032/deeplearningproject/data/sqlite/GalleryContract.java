package wpi.team1032.deeplearningproject.data.sqlite;

import android.provider.BaseColumns;

public class GalleryContract {
    public static final String SQL_CREATE_GALLERY =
            "CREATE TABLE " + GalleryTable.TABLE_NAME + " (" +
                    GalleryTable._ID + " INTEGER PRIMARY KEY," +
                    GalleryTable.COLUMN_NAME_IMG + " BLOB," +
                    GalleryTable.COLUMN_NAME_RATING + " REAL DEFAULT -1);";

    public static final String SQL_DELETE_GALLERY =
            "DROP TABLE IF EXISTS " + GalleryTable.TABLE_NAME;

    private GalleryContract() {};

    public static class GalleryTable implements BaseColumns {
        public static final String TABLE_NAME = "galleryTable";

        public static final String COLUMN_NAME_IMG = "img";
        public static final String COLUMN_NAME_RATING = "rating";
    }
}
