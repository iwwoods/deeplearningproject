package wpi.team1032.deeplearningproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import wpi.team1032.deeplearningproject.data.sqlite.GalleryDbHelper;
import wpi.team1032.deeplearningproject.ui.main.GalleryFragment;

public class MainActivity extends AppCompatActivity {
    GalleryDbHelper mDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, GalleryFragment.newInstance())
                    .commitNow();
        }

        // Instantiate the sqldb
        mDbHelper = GalleryDbHelper.getInstance(this);
    }

    @Override
    protected void onDestroy() {
        mDbHelper.close();
        super.onDestroy();
    }
}
